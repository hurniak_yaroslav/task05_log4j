package com.hurniakyaroslav;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC457d7fc7b0afa2cdda29cae4f4d3d098";
    public static final String AUTH_TOKEN = "a0c9dbd3c6e49313b750bb504753c636";
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380682079553"), /*my phone number*/
                        new PhoneNumber("+12053031924"), str).create(); /*attached to me number*/
    }
}
